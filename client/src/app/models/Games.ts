import { Data } from "@angular/router";

export interface Game {
    id?: number;
    titulo?: string;
    descripcion: string;
    img?: string;
    fecha?: Date;
}